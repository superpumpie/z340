# z340

This repo accompanies the paper on the Zodiac Killer's z340 cipher. The paper can be found [here](http://www.ep.liu.se/ecp/article.asp?issue=158&article=013).

Apologies that this is a bit messy. If you have any questions, email me at tsjuzek -at- posteo -dot- be. 


You need Python3 installed to execute the scripts in this repo. The following scripts are included:


\> python3 create_random.py

This script creates the pseudo files with real English character frequencies. The pseudo-texts have an "fk" in their name. 


N.B.: The real texts have a "tr" in their name. Their origin can be looked up in the file titles.txtx. 


\> python3 reformat.py

For encryption, the files need to be reformated - which is done with this script. 


\> python3 encrypt.py

Encrypts the files. 


\> python3 bi_and_trigram_analysis_new.py

Provides an analysis of the files. 


\> python3 r_code.py

Creates the R code for visualisation. 


\> python3 svm.py

Is used for the support vector machine analysis. As mentioned in the paper, sometimes z340 is classified as a very, very strong many-to-many cipher. 

There is also a script for a k-means analysis that I have played around with (see e.g. my blog entry). 


Further used in the paper, in order to see how z408 comes out vs a pseudo-cipher:

python3 char_freq.py z408.txt z408

python3 bi_and_trigram_analysis_old.py z408.txt z408 z408


python3 char_freq.py otm_fk_02.txt fk_2

python3 bi_and_trigram_analysis_old.py otm_fk_02.txt fk_2 fk_2





