# -*- coding: utf-8 -*-
# tsj, feb 18

import sys, re, os, random, numpy, math
import operator



def main():
  
  output_file_temp = open("x_delta_entropy_bigram_summary.xtxt", "w", encoding="utf8")
  output_file_temp.close()
  output_file_temp = open("x_delta_entropy_trigram_summary.xtxt", "w", encoding="utf8")
  output_file_temp.close()
    
  for input_file_name in os.listdir("."):
    if input_file_name.endswith(".txt"):
      if ("ref_" in input_file_name) or ("otm_" in input_file_name) or ("mtm_" in input_file_name) or ("z340" in input_file_name) or ("z408" in input_file_name):
      
        identifier = input_file_name[:-4]
        output_file_name = identifier
      
        input_file = open(input_file_name, "r", encoding="utf8")
      
        output_file_delta_entropy_bigram = open("x_delta_entropy_bigram_summary.xtxt", "a", encoding="utf8")
        output_file_delta_entropy_trigram = open("x_delta_entropy_trigram_summary.xtxt", "a", encoding="utf8")
      
        cipher = input_file.read()
        cipher_chars = cipher.split(";")
      
        return_list = bi_tri_gram_analysis(cipher_chars)
        entropy_baseline_bi = return_list[0]
        entropy_baseline_tri = return_list[1]
      
        entropy_random_list_bi = []
        entropy_random_list_tri = []
      
        for i in range(0,1000):
          random.shuffle(cipher_chars)
          return_list = bi_tri_gram_analysis(cipher_chars)
          entropy_random_bi = return_list[0]
          entropy_random_tri = return_list[1]
          entropy_random_list_bi.append(entropy_random_bi)
          entropy_random_list_tri.append(entropy_random_tri)
    
        avg_delta_in_entropy_bi = abs(entropy_baseline_bi - numpy.mean(entropy_random_list_bi))
        avg_delta_in_entropy_tri = abs(entropy_baseline_tri - numpy.mean(entropy_random_list_tri))
    
        avg_delta_in_entropy_bi = round(avg_delta_in_entropy_bi, 3)
        avg_delta_in_entropy_tri = round(avg_delta_in_entropy_tri, 3)
    
        output_file_delta_entropy_bigram.write(identifier + ": " + str(avg_delta_in_entropy_bi) + "\n")
        output_file_delta_entropy_trigram.write(identifier + ": " + str(avg_delta_in_entropy_tri) + "\n")
      
        input_file.close()
        output_file_delta_entropy_bigram.close()
        output_file_delta_entropy_trigram.close()




def bi_tri_gram_analysis(cipher_chars):
  
  return_list = []
  
  bigram_count_dct = {}
  trigram_count_dct = {}
  
  bigram_total_count = 0
  trigram_total_count = 0
  
  before_last_char = ""
  last_char = ""
  current_char = ""
  
  for char in cipher_chars:
    current_char = char
    if last_char != "":
      bi_gram = last_char + " " + current_char
      bigram_total_count += 1
      if bi_gram in bigram_count_dct:
        current_bi_count = bigram_count_dct[bi_gram]
        new_bi_count = current_bi_count + 1
        bigram_count_dct[bi_gram] = new_bi_count
      else:
        bigram_count_dct[bi_gram] = 1
    if before_last_char != "" and last_char != "":
      tri_gram = before_last_char + " " + last_char + " " + current_char
      trigram_total_count += 1
      if tri_gram in trigram_count_dct:
        current_tri_count = trigram_count_dct[tri_gram]
        new_tri_count = current_tri_count + 1
        trigram_count_dct[tri_gram] = new_tri_count
      else:
        trigram_count_dct[tri_gram] = 1
    before_last_char = last_char
    last_char = current_char
  
  sum_of_entropy_terms_bi = 0
  
  for element in bigram_count_dct:
    frequency = bigram_count_dct[element] / bigram_total_count
    entropy_term = frequency * math.log10(frequency)
    sum_of_entropy_terms_bi += entropy_term
  
  entropy_bi = sum_of_entropy_terms_bi * -1
  
  return_list.append(entropy_bi)
  
  
  sum_of_entropy_terms_tri = 0
  
  for element in trigram_count_dct:
    frequency = trigram_count_dct[element] / trigram_total_count
    entropy_term = frequency * math.log10(frequency)
    sum_of_entropy_terms_tri += entropy_term
  
  entropy_tri = sum_of_entropy_terms_tri * -1
  
  return_list.append(entropy_tri)
  
  return(return_list)
  
  
  

# boilerplate
main()