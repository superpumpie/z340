# -*- coding: utf-8 -*-
# tsj, feb 18

import sys, re, os
import operator



def main():
	
	input_file_name = sys.argv[1]
	output_file_name = sys.argv[2]
	identifier = sys.argv[3]
	
	input_file = open(input_file_name, "r", encoding="utf8")
	output_file_bigram_summary = open("a_bigram_summary.txt", "a", encoding="utf8")
	output_file_bigram_verbose = open("a_bigram_verbose.txt", "a", encoding="utf8")
	output_file_trigram_summary = open("a_trigram_summary.txt", "a", encoding="utf8")
	output_file_trigram_verbose = open("a_trigram_verbose.txt", "a", encoding="utf8")
	output_file_MSD_bigram = open("a_msd_bigram_" + output_file_name + "_summary.txt", "a", encoding="utf8")
	output_file_MSD_trigram = open("a_msd_trigram_" + output_file_name + "_summary.txt", "a", encoding="utf8")
	
	
	output_file_bigram_verbose.write(input_file_name + "\n")
	output_file_trigram_verbose.write(input_file_name + "\n")
	
	bigram_count_dct = {}
	trigram_count_dct = {}
	
	bigram_total_count = 0
	trigram_total_count = 0
	
	cipher = input_file.read()
	
	cipher_chars = cipher.split(";")
	
	before_last_char = ""
	last_char = ""
	current_char = ""
	
	for char in cipher_chars:
		current_char = char
		if last_char != "":
			bi_gram = last_char + " " + current_char
			bigram_total_count += 1
			if bi_gram in bigram_count_dct:
				current_bi_count = bigram_count_dct[bi_gram]
				new_bi_count = current_bi_count + 1
				bigram_count_dct[bi_gram] = new_bi_count
			else:
				bigram_count_dct[bi_gram] = 1
		if before_last_char != "" and last_char != "":
			tri_gram = before_last_char + " " + last_char + " " + current_char
			trigram_total_count += 1
			if tri_gram in trigram_count_dct:
				current_tri_count = trigram_count_dct[tri_gram]
				new_tri_count = current_tri_count + 1
				trigram_count_dct[tri_gram] = new_tri_count
			else:
				trigram_count_dct[tri_gram] = 1
		before_last_char = last_char
		last_char = current_char
	
	sum_of_squared_differences = 0
	output_string_bi_summary = identifier + "_bi <- c("
	#modified from https://stackoverflow.com/questions/613183/how-do-i-sort-a-dictionary-by-value
	for key, value in sorted(bigram_count_dct.items(), key=operator.itemgetter(1), reverse = True):
		rel_freq = (float(value) / float(bigram_total_count)) * 100
		rel_freq = round(rel_freq, 2)
		output_string_bi_summary += str(value) + ", "
		output_file_bigram_verbose.write(key + "\t" + str(value) + "\t" + str(rel_freq) + "\n")
		difference_to_one = value - 1
		difference_to_one_squared = difference_to_one * difference_to_one
		sum_of_squared_differences += difference_to_one_squared
	
	mean_of_squared_differences = sum_of_squared_differences / bigram_total_count
	mean_of_squared_differences = round(mean_of_squared_differences, 3)
	
	output_string_bi_summary = output_string_bi_summary[:-2] + ")\n"
	output_file_bigram_summary.write(output_string_bi_summary)
	
	output_file_bigram_verbose.write("\n")
	
	output_file_MSD_bigram.write(identifier + ": " + str(mean_of_squared_differences) + "\n")
	
	
	sum_of_squared_differences = 0
	output_string_tri_summary = identifier + "_tri <- c("
	#modified from https://stackoverflow.com/questions/613183/how-do-i-sort-a-dictionary-by-value
	for key, value in sorted(trigram_count_dct.items(), key=operator.itemgetter(1), reverse = True):
		rel_freq = (float(value) / float(trigram_total_count)) * 100
		rel_freq = round(rel_freq, 2)
		output_string_tri_summary += str(value) + ", "
		output_file_trigram_verbose.write(key + "\t" + str(value) + "\t" + str(rel_freq) + "\n")
		difference_to_one = value - 1
		difference_to_one_squared = difference_to_one * difference_to_one
		sum_of_squared_differences += difference_to_one_squared
		
	mean_of_squared_differences = sum_of_squared_differences / bigram_total_count
	mean_of_squared_differences = round(mean_of_squared_differences, 3)
	
	output_string_tri_summary = output_string_tri_summary[:-2] + ")\n"
	output_file_trigram_summary.write(output_string_tri_summary)
	
	output_file_trigram_verbose.write("\n")
	
	output_file_MSD_trigram.write(identifier + ": " + str(mean_of_squared_differences) + "\n")
	
	
	input_file.close()
	output_file_bigram_summary.close()
	output_file_bigram_verbose.close()
	output_file_trigram_summary.close()
	output_file_trigram_verbose.close()


# boilerplate
main()