# -*- coding: utf-8 -*-
# tsj, feb 18

import sys, re, os, random
import operator



def main():
	
	for input_file_name in os.listdir("."):
		if input_file_name.endswith(".txt"):
			if "raw" in input_file_name:
				input_file = open(input_file_name, "r", encoding="utf8")
				output_file_name = re.sub('raw', 'ref', input_file_name)

				output_file = open(output_file_name, "w", encoding="utf8")

				output_string = ""

				for line in input_file:
					line = line.strip()
					line = line.upper()
					line = re.sub(r'[^A-Z]', "", line) 
					for char in line:
						output_string += char + ";"

				output_string = output_string[:-1]
				output_file.write(output_string)

				input_file.close()
				output_file.close()
		
	

# boilerplate
main()