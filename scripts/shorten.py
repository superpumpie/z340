# -*- coding: utf-8 -*-
# tsj, feb 18

import sys, re, os, random
import operator



def main():
	
	for input_file_name in os.listdir("/home/tommy/Bureaublad/d-msds/all_data_sets_shorten/"):
	
		input_file = open(input_file_name, "r", encoding="utf8")
		input_file_string = input_file.read()
		input_file.close()
		
		output_file_name = input_file_name
	
		output_file = open(output_file_name, "w", encoding="utf8")
	
		output_string = ""
		
		random_number = random.randint(15,25)
		random_length = (17 * random_number)
		
		for char in input_file_string:
			output_string += char
	
		output_string = output_string[:random_length]
		output_file.write(output_string)
		
		output_file.close()
		
	

# boilerplate
main()