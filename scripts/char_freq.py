# -*- coding: utf-8 -*-
# tsj, feb 18

import sys, re, os
import operator



def main():
	
	input_file_name = sys.argv[1]
	identifier = sys.argv[2]
	
	input_file = open(input_file_name, "r", encoding="utf8")
	output_file_summary = open("a_char_freq_summary.txt", "a", encoding="utf8")
	output_file_verbose = open("a_char_freq_verbose.txt", "a", encoding="utf8")
	
	output_file_verbose.write(input_file_name + "\n")
	
	char_count_dct = {}
	
	total_count = 0
	
	for line in input_file:
		new_line = line.strip()
		chars = new_line.split(";")
		for char in chars:
			total_count += 1
			if char in char_count_dct:
				current_count = char_count_dct[char]
				new_count = current_count + 1
				char_count_dct[char] = new_count
			else:
				char_count_dct[char] = 1
	
	output_string_summary = identifier + "_cf <- c("
	#modified from https://stackoverflow.com/questions/613183/how-do-i-sort-a-dictionary-by-value
	for key, value in sorted(char_count_dct.items(), key=operator.itemgetter(1), reverse = True):
		rel_freq = (float(value) / float(total_count)) * 100
		rel_freq = round(rel_freq, 2)
		output_string_summary += str(rel_freq) + ", "
		output_file_verbose.write(key + "\t" + str(value) + "\t" + str(rel_freq) + "\n")
	
	output_string_summary = output_string_summary[:-2] + ")\n\n"
	output_file_summary.write(output_string_summary)
	
	output_file_verbose.write("\n\n")
	
	input_file.close()
	output_file_summary.close()
	output_file_verbose.close()


# boilerplate
main()