# -*- coding: utf-8 -*-
# tsj, feb 18

import sys, re, os, random
import operator



def main():	
	min_length = 425
	max_length = 0
	for i in range(1, 65):
	
		cipher_length = random.randint(15, 25) * 17
		if cipher_length < min_length:
			min_length = cipher_length
		if cipher_length > max_length:
			max_length = cipher_length
		oi = str(i)
		if i < 10:
			oi = "0" + oi
		output_file = open("raw_fk_" + oi + ".txt", "w", encoding="utf8")

		draw_to_letter = {}

		letters_by_freq = ["E", "T", "A", "O", "I", "N", "S", "H", "R", "D", "L", "C", "U", "M", "W", "F", "G", "Y", "P", "B", "V", "K", "J", "X", "Q", "Z"]
		relative_freq_of_letters = [13, 9, 8, 8, 7, 7, 6, 6, 6, 4, 4, 3, 3, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1]

		counter = 0
		for i in range(0,26):
			letter = letters_by_freq[i]
			letter_freq = relative_freq_of_letters[i]
			for j in range(0,letter_freq):
				draw_to_letter[counter] = letter
				counter += 1

		output_string = ""

		for _ in range(0,cipher_length):
			random_draw = random.randint(0,102)
			random_letter = draw_to_letter[random_draw]
			output_string += random_letter

		output_file.write(output_string)

		output_file.close()
	
	print(min_length)
	print(max_length)

	

# boilerplate
main()