# -*- coding: utf-8 -*-
# tsj, feb 18

import sys, re, os, random
import operator



def main():
	output_file_length = open("x_length.xtxt", "w", encoding="utf8")
	#for input_file_name in os.listdir("/home/tommy/Bureaublad/d-msds/scripts/"):
	for input_file_name in os.listdir("."):
		if input_file_name.endswith(".txt"):
			if "ref" in input_file_name:
			
				output_file_single_name = re.sub('ref', 'otm', input_file_name)
				output_file_double_name = re.sub('ref', 'mtm', input_file_name)
				collapse_etao_to_one = 0
	
				input_file = open(input_file_name, "r", encoding="utf8")
				output_file_single = open(output_file_single_name, "w", encoding="utf8")
				output_file_double = open(output_file_double_name, "w", encoding="utf8")

				clear_text = input_file.read()
				if clear_text[-1] == ";":
					clear_text = clear_text[:-1]

				char_frequency_dct = get_char_frequency(clear_text)

				thresholds = [2, 4, 6 , 8.5]
				for i in range(0,2):
					thresholds[i] = thresholds[i] + random.uniform(-0.5, 0.5)
				for i in range(2,4):
					thresholds[i] = thresholds[i] + random.uniform(-1, 1)
				single_layer_char_mappings_dct = get_first_layer_char_mappings(char_frequency_dct, thresholds, collapse_etao_to_one)
				output_file_length.write("single layer char set:\t" + str(single_layer_char_mappings_dct["len"]) + "\n")
				del single_layer_char_mappings_dct["len"]
				single_layer_encryption_string = get_encryption(clear_text, single_layer_char_mappings_dct)

				thresholds = [2, 6, 11, 17]
				for i in range(0,4):
					thresholds[i] = thresholds[i] + random.uniform(-0.5, 0.5)
				double_layer_first_char_mappings_dct = get_first_layer_char_mappings(char_frequency_dct, thresholds, collapse_etao_to_one)
				#output_file_length.write("double layer char set first layer: " + str(double_layer_first_char_mappings_dct["len"]) + "\n")
				del double_layer_first_char_mappings_dct["len"]
				double_layer_second_char_mappings_dct = get_second_layer_char_mappings(double_layer_first_char_mappings_dct)
				output_file_length.write("double layer char set second layer:\t" + str(double_layer_second_char_mappings_dct["len"]) + "\n")
				del double_layer_second_char_mappings_dct["len"]
				double_layer_encryption_string = get_encryption(clear_text, double_layer_second_char_mappings_dct)	

				output_file_single.write(single_layer_encryption_string)
				output_file_double.write(double_layer_encryption_string)

				input_file.close()
				output_file_single.close()
				output_file_double.close()
	output_file_length.close()


def double_layer_encryption(single_layer_encryption_string, char_mappings_dct):
	double_layer_encryption_string = ""
	single_layer_encryption_string_list = single_layer_encryption_string.split(";")
	for char in single_layer_encryption_string_list:
		symbol_candidates_string = char_mappings_dct[char]
		symbol_candidates_list = symbol_candidates_string.split(";")
		number_of_candidates = len(symbol_candidates_list) - 1
		random_candidate_number = random.randint(0,number_of_candidates)
		random_candidate = symbol_candidates_list[random_candidate_number]
		double_layer_encryption_string += random_candidate + ";"
	double_layer_encryption_string = double_layer_encryption_string[:-1]
	return(double_layer_encryption_string)


def get_second_layer_char_mappings(double_layer_first_char_mappings_dct):
	length_of_character_set = random.randint(60,64)
	new_mapping_candidates_list = list(range(0, length_of_character_set))
	total_number_of_mappings = 0
	threshold_1 = 8 + random.randint(-1,1)
	threshold_2 = 15 + random.randint(-1,1)
	threshold_3 = 19 + random.randint(-1,1)
	if threshold_3 <= (threshold_2 + 1):
		threshold_3 = (threshold_2 + 1)
	
	for char in double_layer_first_char_mappings_dct:
		new_char_mappings_string = ""
		char_mappings_string = double_layer_first_char_mappings_dct[char]
		char_mappings_list = char_mappings_string.split(";")
		for char_mapping in char_mappings_list:
			random_number = random.randint(1,20)
			if 1 <= random_number <= threshold_1:
				number_of_mappings = 1
			if (threshold_1 + 1) <= random_number <= threshold_2:
				number_of_mappings = 2
			if (threshold_2 +1) <= random_number <= threshold_3:
				number_of_mappings = 3
			if (threshold_3 + 1) <= random_number <= 20:
				number_of_mappings = 4
			for _ in range(0,number_of_mappings):
				total_number_of_mappings += 1
				reduced_length_new_mapping_candidates_list = len(new_mapping_candidates_list) - 1
				random_mapping_candidate = random.randint(0,reduced_length_new_mapping_candidates_list)
				random_mapping = new_mapping_candidates_list[random_mapping_candidate]
				new_char_mappings_string += str(random_mapping) + ";"
				del new_mapping_candidates_list[random_mapping_candidate]
				if len(new_mapping_candidates_list) == 0:
					new_mapping_candidates_list = list(range(0, length_of_character_set))
		new_char_mappings_string = new_char_mappings_string[:-1]
		double_layer_first_char_mappings_dct[char] = new_char_mappings_string
	if total_number_of_mappings > length_of_character_set:
		total_number_of_mappings = length_of_character_set
	double_layer_first_char_mappings_dct["len"] = total_number_of_mappings
	
	return(double_layer_first_char_mappings_dct)



def get_encryption(clear_text, char_mappings_dct):
	encryption_string = ""
	clear_test_list = clear_text.split(";")
	for char in clear_test_list:
		symbol_candidates_string = char_mappings_dct[char]
		symbol_candidates_list = symbol_candidates_string.split(";")
		number_of_candidates = len(symbol_candidates_list) - 1
		random_candidate_number = random.randint(0,number_of_candidates)
		random_candidate = symbol_candidates_list[random_candidate_number]
		encryption_string += random_candidate + ";"
	encryption_string = encryption_string[:-1]
	return(encryption_string)


def get_first_layer_char_mappings(char_frequency_dct, thresholds, collapse_etao_to_one):
	char_mappings_dct = {}
	next_available_symbol = 0
	for char in char_frequency_dct:
		freq = char_frequency_dct[char]
		no_of_mappings = 1
		if freq > thresholds[0]:
			no_of_mappings = 2
		if freq > thresholds[1]:
			no_of_mappings = 3
		if freq > thresholds[2]:
			no_of_mappings = 4
		if freq > thresholds[3]:
			no_of_mappings = 5
		
		char_mappings = ""
		for _ in range(0,no_of_mappings):
			char_mappings += str(next_available_symbol) + ";"
			next_available_symbol +=1
		char_mappings = char_mappings[:-1]
		char_mappings_dct[char] = char_mappings
	char_mappings_dct["len"] = next_available_symbol - 1
	if collapse_etao_to_one == 1:
		candidates_to_collapse = ["E", "T", "A", "O"]
		random_candidate_number = random.randint(0,3)
		random_candidate = candidates_to_collapse[random_candidate_number]
		code_for_etao = char_mappings_dct[random_candidate]
		code_for_etao_elements = code_for_etao.split(";")
		code_for_etao_elements_first = code_for_etao_elements[0]
		new_etao_string = ""
		counter = 0
		for code_for_etao_element in code_for_etao_elements:
			if counter < 3:
				new_etao_string += code_for_etao_elements_first + ";"
			else:
				new_etao_string += code_for_etao_element + ";"
			counter += 1
		new_etao_string = new_etao_string[:-1]
		char_mappings_dct[random_candidate] = new_etao_string
	return(char_mappings_dct)


def get_char_frequency(clear_text):
	char_count_dct = {}
	char_rel_freq_dct = {}
	total_count = 0
	
	chars = clear_text.split(";")
	for char in chars:
		total_count += 1
		if char in char_count_dct:
			current_count = char_count_dct[char]
			new_count = current_count + 1
			char_count_dct[char] = new_count
		else:
			char_count_dct[char] = 1
	
	#modified from https://stackoverflow.com/questions/613183/how-do-i-sort-a-dictionary-by-value
	for key, value in sorted(char_count_dct.items(), key=operator.itemgetter(1), reverse = True):
		rel_freq = (float(value) / float(total_count)) * 100
		rel_freq = round(rel_freq, 2)
		
		char_rel_freq_dct[key] = rel_freq
	
	return(char_rel_freq_dct)




# boilerplate
main()