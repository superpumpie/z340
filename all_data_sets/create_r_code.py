# -*- coding: utf-8 -*-
# tsj, feb 18

import sys, re, os, random
import operator



def main():
	
	
	input_file_bi = open("x_delta_entropy_bigram_summary.xtxt", "r", encoding="utf8")
	input_file_tri = open("x_delta_entropy_trigram_summary.xtxt", "r", encoding="utf8")
	input_file_bi_string = input_file_bi.read()
	input_file_tri_string = input_file_tri.read()
	input_file_bi.close()
	input_file_tri.close()
	
	
	sorted_bi_list = sort_entries(input_file_bi_string)
	sorted_tri_list = sort_entries(input_file_tri_string)
	
	output_string = ""
	output_string_k = ""
	
	for i in range(0,386):
		if i == 0:
			output_string += "### fake ciphers 1-32 mtm\n"
			colour = "red"
			output_string_k += "\n\n### fake ciphers 1-32 mtm\n"
		if i == 32:
			output_string += "### fake ciphers 33-64 mtm\n"
			output_string_k += "\n\n### fake ciphers 33-64 mtm\n"
		if i == 64:
			output_string += "### true ciphers 1-32 mtm\n"
			colour = "gold2"
			output_string_k += "\n\n### true ciphers 1-32 mtm\n"
		if i == 96:
			output_string += "### true ciphers 33-64 mtm\n"
			output_string_k += "\n\n### true ciphers 33-64 mtm\n"
		if i == 128:
			output_string += "### fake ciphers 1-32 otm\n"
			colour = "red"
			output_string_k += "\n\n### fake ciphers 1-32 otm\n"
		if i == 160:
			output_string += "### fake ciphers 33-64 otm\n"
			output_string_k += "\n\n### fake ciphers 33-64 otm\n"
		if i == 192:
			output_string += "### true ciphers 1-32 otm\n"
			colour = "gold2"
			output_string_k += "\n\n### true ciphers 1-32 otm\n"
		if i == 224:
			output_string += "### true ciphers 33-64 otm\n"
			output_string_k += "\n\n### true ciphers 33-64 otm\n"
		if i == 256:
			output_string += "### fake plain texts 01-32\n"
			colour = "red"
			output_string_k += "\n\n### fake plain texts 01-32\n"
		if i == 288:
			output_string += "### fake plain texts 33-64\n"
			output_string_k += "\n\n### fake plain texts 33-64\n"
		if i == 320:
			output_string += "### true plain texts 01-32\n"
			colour = "gold2"
			output_string_k += "\n\n### true plain texts 01-32\n"
		if i == 352:
			output_string += "### true plain texts 33-64\n"
			output_string_k += "\n\n### true plain texts 33-64\n"
		if i == 384:
			output_string += "### z340\n"
			colour = "gray"
			output_string_k += "\n\n### z340\n"
		if i == 385:
			output_string += "### z408\n"
			output_string_k += "\n\n### z408\n"
		sorted_bi_list_elements = sorted_bi_list[i].split(" ")
		sorted_tri_list_elements = sorted_tri_list[i].split(" ")
		output_string += "points(" + sorted_bi_list_elements[1] + ", " + sorted_tri_list_elements[1] + ", pch = 18, col = \"" + colour + "\", cex = 1.75)\n"
		output_string_k += "[" + sorted_bi_list_elements[1] + ", " + sorted_tri_list_elements[1] + "], "
	
	
	output_file = open("x_entropy_r_code.xtxt", "w", encoding="utf8")
	output_file.write(output_string + "\n\n" + output_string_k)
	output_file.close()


def sort_entries(input_string):
	input_string_as_list = input_string.split("\n")
	id_and_values = []
	for line in input_string_as_list:
		if len(line) > 0:
			id_and_values.append(line)
	id_and_values.sort()
	return(id_and_values)
	

# boilerplate
main()